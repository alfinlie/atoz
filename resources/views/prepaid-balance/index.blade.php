@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Prepaid Balance</div>

                <div class="card-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    @endif

                    <form action="{{ route('prepaid-balance.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <input class="form-control" type="number" name="mobile_number" placeholder="Mobile Number">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="number" name="value" placeholder="Value">
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
