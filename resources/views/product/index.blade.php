@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Product</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="index.html" method="post">
                        <div class="form-group">
                            <textarea class="form-control" name="name" rows="3" placeholder="Product"></textarea>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="address" rows="3" placeholder="Shipping Address"></textarea>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" name="price" placeholder="Price">
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
