@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h3>
                        @switch($data['transaction']->payment_status)
                            @case('waiting')
                                Success!
                                @break
                            @case('success')
                                Success!
                                @break
                            @case('failed')
                                Failed!
                                @break
                            @default
                                Canceled!
                        @endswitch
                    </h3>
                </div>

                <div class="card-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>Order no.</td>
                            <td class="text-right">{{ $data['transaction']->order_no }}</td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td class="text-right">Rp {{ number_format($data['transaction']->total_payment,0,",",".") }}</td>
                        </tr>
                        @if (isset($data['product']))
                            <tr>
                                <td colspan="2">
                                    {{ $data['product']['product'] }} that costs {{ $data['product']['price'] }} will be shipped to:
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    {{ $data['product']['shipping_address'] }}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    only after you pay.
                                </td>
                            </tr>
                        @endif
                        @if ($data['transaction']->payment_status == 'waiting')
                            <tr>
                                <td colspan="2">
                                    <button class="btn btn-primary btn-block" type="button">Pay Now</button>
                                </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
