<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', function() {
        return redirect()->route('prepaid-balance.index');
    });

    Route::get('/prepaid-balance', 'PrepaidBalanceController@index')->name('prepaid-balance.index');
    Route::post('/prepaid-balance', 'PrepaidBalanceController@store')->name('prepaid-balance.store');

    Route::get('/product', 'ProductController@index')->name('product.index');

    Route::get('/success/{transaction_id}', 'TransactionController@success')->name('transaction.success');
});
