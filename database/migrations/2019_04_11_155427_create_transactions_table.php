<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->enum('type', ['prepaid_balance', 'product']);
            $table->enum('payment_status', ['success', 'failed', 'canceled', 'waiting']);
            $table->dateTime('paid_at')->nullable();
            $table->string('order_no')->unique();
            $table->string('total_payment');
            $table->string('shipping_code')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
