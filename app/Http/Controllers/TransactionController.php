<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TransactionRepositoryInterface;

class TransactionController extends Controller
{
    public function __construct(TransactionRepositoryInterface $transactionRepositoryInterface,
                                Request $request)
    {
        $this->transactionRepositoryInterface = $transactionRepositoryInterface;
        $this->request = $request;
    }

    public function success($transaction_id)
    {
        $data['transaction'] = $this->transactionRepositoryInterface->findById($transaction_id);

        return view('transaction.success')->with('data', $data);
    }
}
