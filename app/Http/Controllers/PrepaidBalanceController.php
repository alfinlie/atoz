<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\PrepaidBalanceRepositoryInterface;
use App\Repositories\TransactionRepositoryInterface;
use Carbon\Carbon;
use DB;

class PrepaidBalanceController extends Controller
{
    public function __construct(PrepaidBalanceRepositoryInterface $prepaidBalanceRepositoryInterface,
                                TransactionRepositoryInterface $transactionRepositoryInterface,
                                Request $request)
    {
        $this->prepaidBalanceRepositoryInterface = $prepaidBalanceRepositoryInterface;
        $this->transactionRepositoryInterface = $transactionRepositoryInterface;
        $this->request = $request;
    }

    public function index()
    {
        return view('prepaid-balance.index');
    }

    public function store()
    {
        $this->validate($this->request, [
            'mobile_number' => 'required|numeric|digits_between:7,12|regex:/^081/',
            'value' => 'required|in:10000,50000,100000'
        ]);

        $data['prepaid_balance'] = $this->request->all();
        $data['prepaid_balance']['user_id'] = Auth::user()->id;
        $response['prepaid_balance'] = $this->prepaidBalanceRepositoryInterface->store($data['prepaid_balance']);

        $data['transaction']['order_id'] = $response['prepaid_balance']['id'];
        $data['transaction']['type'] = 'prepaid_balance';
        $data['transaction']['payment_status'] = $this->checkSuccess();
        $data['transaction']['order_no'] = str_shuffle(strtotime(Carbon::now()));
        $data['transaction']['total_payment'] = $this->calcTotal($response['prepaid_balance']['value']);
        $response['transaction'] = $this->transactionRepositoryInterface->store($data['transaction']);

        return redirect()->route('transaction.success', $response['transaction']['id']);
    }

    private function checkSuccess()
    {
        $random = rand(1,100);
        $current_time = Carbon::now()->format('H:i:s');
        $start_raw = "09:00:00";
        $end_raw = "17:00:00";
        $start = Carbon::createFromFormat('H:i:s', $start_raw);
        $end = Carbon::createFromFormat('H:i:s', $end_raw);

        if ($current_time > $start && $current_time < $end)
        {
            if($random > 10) {
                return 'waiting';
            }
        } else {
            if($random > 40) {
                return 'waiting';
            }
        }
        return 'failed';
    }

    private function calcTotal($value)
    {
        return $value + ($value * 5 / 100);
    }
}
