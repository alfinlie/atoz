<?php

namespace App\Repositories;

use App\PrepaidBalance;

class PrepaidBalanceRepository implements PrepaidBalanceRepositoryInterface
{
	protected $prepaidBalance;

	public function __construct(PrepaidBalance $prepaidBalance)
	{
	    $this->prepaidBalance = $prepaidBalance;
    }

    public function store(array $data)
    {
        return $this->prepaidBalance->create($data);
    }
}
