<?php

namespace App\Repositories;

use App\Transaction;

class TransactionRepository implements TransactionRepositoryInterface
{
	protected $transaction;

	public function __construct(Transaction $transaction)
	{
	    $this->transaction = $transaction;
    }

    public function store(array $data)
    {
        return $this->transaction->create($data);
    }

    public function findById($id)
    {
        return $this->transaction->find($id);
    }
}
