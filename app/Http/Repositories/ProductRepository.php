<?php

namespace App\Repositories;

use App\Product;

class TransactionRepository implements TransactionRepositoryInterface
{
	protected $product;

	public function __construct(Product $product)
	{
	    $this->product = $product;
    }

    public function store(array $data)
    {
        return $this->product->create($data);
    }

    public function findById($id)
    {
        return $this->product->find($id);
    }
}
