<?php

namespace App\Repositories;

interface TransactionRepositoryInterface
{
    public function store(array $data);
    public function findById($id);
}
