<?php

namespace App\Repositories;

interface ProductRepositoryInterface
{
    public function store(array $data);
    public function findById($id);
}
