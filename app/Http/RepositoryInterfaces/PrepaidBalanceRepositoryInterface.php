<?php

namespace App\Repositories;

interface PrepaidBalanceRepositoryInterface
{
    public function store(array $data);
}
