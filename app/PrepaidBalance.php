<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrepaidBalance extends Model
{
    protected $table = 'prepaid_balances';
    protected $fillable = [
        'user_id',
        'mobile_number',
        'value'
    ];
}
