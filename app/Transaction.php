<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $fillable = [
        'id',
        'order_id',
        'type',
        'payment_status',
        'paid_at',
        'order_no',
        'total_payment',
        'shipping_code'
    ];
}
